﻿#region copyright

//------------------------------------------------------------------------------
//  此代码版权声明为全文件覆盖，如有原作者特别声明，会在下方手动补充
//  此代码版权（除特别声明外的代码）归作者本人Diego所有
//  源代码使用协议遵循本仓库的开源协议及附加协议
//  Gitee源代码仓库：https://gitee.com/diego2098/ThingsGateway
//  Github源代码仓库：https://github.com/kimdiego2098/ThingsGateway
//  使用文档：https://diego2098.gitee.io/thingsgateway-docs/
//  QQ群：605534569
//------------------------------------------------------------------------------

#endregion

using BlazorComponent;

using Mapster;

using Masa.Blazor;

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;

using SqlSugar;

using ThingsGateway.Admin.Blazor;

namespace ThingsGateway.Gateway.Blazor;

/// <summary>
/// 采集设备页面
/// </summary>
public partial class CollectDevicePage : BaseComponentBase
{
    private readonly DevicePageInput _search = new();
    private IAppDataTable _datatable;
    private List<string> _deviceGroups = new();
    private List<CollectDevice> _devices = new();
    private List<DriverPlugin> _driverPlugins;
    private ImportExcel _importExcel;

    //string _searchName;
    [Inject]
    private AjaxService _ajaxService { get; set; }

    [Inject]
    private DriverPluginService _driverPluginService { get; set; }

    [CascadingParameter]
    private MainLayout _mainLayout { get; set; }

    /// <inheritdoc/>
    protected override async Task OnParametersSetAsync()
    {
        Refresh();
        _driverPlugins = _driverPluginService.GetAllDriverPlugin(DriverEnum.Collect);
        await base.OnParametersSetAsync();
    }

    private async Task GetDriverProperties(List<CollectDevice> data)
    {
        if (data != null)
        {
            if (!data.Any())
            {
                await PopupService.EnqueueSnackbarAsync("需选择一项或多项", AlertTypes.Warning);
                return;
            }
        }
        data ??= _serviceScope.ServiceProvider.GetService<ICollectDeviceService>().GetCacheList(true);
        foreach (var device in data)
        {
            device.DevicePropertys = GetDriverProperties(device.PluginName, device.Id);
        }
        await _serviceScope.ServiceProvider.GetService<ICollectDeviceService>().EditsAsync(data);
        await PopupService.EnqueueSnackbarAsync("刷新成功", AlertTypes.Success);
    }

    private async Task AddCallAsync(DeviceAddInput input)
    {
        await _serviceScope.ServiceProvider.GetService<ICollectDeviceService>().AddAsync(input);
        Refresh();
        await _mainLayout.StateHasChangedAsync();
    }

    private async Task CopyDevAndVarAsync(IEnumerable<CollectDevice> data)
    {
        if (!data.Any())
        {
            await PopupService.EnqueueSnackbarAsync("需选择一项或多项", AlertTypes.Warning);
            return;
        }

        await _serviceScope.ServiceProvider.GetService<ICollectDeviceService>().CopyDevAndVarAsync(data);
        await DatatableQueryAsync();
        await PopupService.EnqueueSnackbarAsync("复制成功", AlertTypes.Success);
        await _mainLayout.StateHasChangedAsync();
    }

    private async Task CopyDeviceAsync(IEnumerable<CollectDevice> data)
    {
        if (!data.Any())
        {
            await PopupService.EnqueueSnackbarAsync("需选择一项或多项", AlertTypes.Warning);
            return;
        }

        await _serviceScope.ServiceProvider.GetService<ICollectDeviceService>().CopyDevAsync(data);
        await DatatableQueryAsync();
        await PopupService.EnqueueSnackbarAsync("复制成功", AlertTypes.Success);
        await _mainLayout.StateHasChangedAsync();
    }

    private async Task DatatableQueryAsync()
    {
        await _datatable?.QueryClickAsync();
    }

    private async Task DeleteCallAsync(IEnumerable<CollectDevice> input)
    {
        await _serviceScope.ServiceProvider.GetService<ICollectDeviceService>().DeleteAsync(input.Select(a => a.Id).ToArray());
        Refresh();
        await _mainLayout.StateHasChangedAsync();
    }

    private Task<Dictionary<string, ImportPreviewOutputBase>> DeviceImportAsync(IBrowserFile file)
    {
        return _serviceScope.ServiceProvider.GetService<ICollectDeviceService>().PreviewAsync(file);
    }

    private async Task DownExportAsync(DevicePageInput input = null)
    {
        await _ajaxService.DownFileAsync("gatewayFile/collectDevice", DateTimeExtensions.CurrentDateTime.ToFileDateTimeFormat(), input.Adapt<DeviceInput>());
    }

    private async Task DriverValueChangedAsync(DeviceAddInput context, string pluginName)
    {
        if (pluginName.IsNullOrEmpty()) return;

        if (context.DevicePropertys == null || context.DevicePropertys?.Count == 0 || context.PluginName != pluginName)
        {
            try
            {
                var currentDependencyProperty = GetDriverProperties(pluginName, context.Id);
                context.DevicePropertys = currentDependencyProperty;
                await PopupService.EnqueueSnackbarAsync("插件附加属性已更新", AlertTypes.Success);
            }
            catch (Exception ex)
            {
                await PopupService.EnqueueSnackbarAsync(ex);
            }
        }
        context.PluginName = pluginName;
    }

    private async Task EditCallAsync(DeviceEditInput input)
    {
        await _serviceScope.ServiceProvider.GetService<ICollectDeviceService>().EditAsync(input);
        _devices = _serviceScope.ServiceProvider.GetService<ICollectDeviceService>().GetCacheList(true);
        _deviceGroups = _devices?.Select(a => a.DeviceGroup)?.Where(a => a != null).Distinct()?.ToList();
        await _mainLayout.StateHasChangedAsync();
    }

    private List<DependencyProperty> GetDriverProperties(string pluginName, long devId)
    {
        return BackgroundServiceUtil.GetBackgroundService<CollectDeviceWorker>().GetDevicePropertys(pluginName, devId);
    }

    private async Task<ISqlSugarPagedList<CollectDevice>> QueryCallAsync(DevicePageInput input)
    {
        var data = await _serviceScope.ServiceProvider.GetService<ICollectDeviceService>().PageAsync(input);
        return data;
    }

    private void Refresh()
    {
        _devices = _serviceScope.ServiceProvider.GetService<ICollectDeviceService>().GetCacheList(true);
        _deviceGroups = _devices?.Select(a => a.DeviceGroup)?.Where(a => a != null).Distinct()?.ToList();
    }

    private async Task SaveDeviceImportAsync(Dictionary<string, ImportPreviewOutputBase> data)
    {
        await _serviceScope.ServiceProvider.GetService<ICollectDeviceService>().ImportAsync(data);
        await DatatableQueryAsync();
        _importExcel.IsShowImport = false;
        await _mainLayout.StateHasChangedAsync();
    }
}